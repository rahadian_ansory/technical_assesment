package technical.assesment

import java.io.{BufferedWriter, File, FileWriter}
import scala.collection.immutable.TreeMap

/**
  * Created by iqbal on 28/01/18.
  */
object assesment2 {

  def main(args: Array[String]): Unit = {

    val inputFile = List(
      "./assesment2/age1.txt",
      "./assesment2/age2.txt"
    )

    val ageMap:TreeMap[Int,Int] = inputFile.foldLeft[TreeMap[Int,Int]](TreeMap.empty){
      case (localMemo,path) =>

        val bufferedSource = io.Source.fromFile(path)

        val localResult = bufferedSource.getLines().foldLeft[TreeMap[Int,Int]](localMemo) {
          case (memo, line) =>
            try {

              val age = line.toInt

              memo.get(age) match {
                case Some(data) =>
                  val newCount = data + 1
                  memo + (age -> newCount)
                case None =>
                  memo + (age -> 1)
              }

            } catch {
              case e: Exception =>
                memo
            }
        }

        bufferedSource.close()
        localResult
    }

    val file = new File("./sorted_age.txt")
    val bw = new BufferedWriter(new FileWriter(file))

    ageMap.foreach{
      case (key,num) =>
        for (i <- 1 to num){
          bw.write(s"""${key.toString}\n""")
        }
    }

    bw.close()

    println(s"FINISH CREATING sorted_age.txt at ${file.getCanonicalPath}")

  }
}
