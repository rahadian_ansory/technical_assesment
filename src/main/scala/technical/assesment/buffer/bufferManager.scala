package technical.assesment.buffer

import java.io.File
import technical.assesment.dataStructure.userPreference
import scala.collection.mutable

/**
  * Created by iqbal on 22/01/18.
  */
object bufferManager {

  private var productMap:mutable.Map[Int,productDef] = mutable.Map.empty
  private var userMap:mutable.Map[Int,userDef] = mutable.Map.empty
  private var fileMap:mutable.Map[String,Boolean] = mutable.Map.empty

  def scanFolder(path:String): List[String] ={

    val d = new File(path)

    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList.map(_.getAbsolutePath)
    } else {
      List.empty
    }

  }

  def getUserPreference(uid:Int): List[userPreference] ={
    userMap.get(uid) match {
      case Some(data) =>
        data.dataList
      case None =>
        List.empty
    }
  }

  def getProductScore(pid:Int) : Int ={
    productMap.get(pid) match{
      case Some(data) =>
        data.score
      case None =>
        0
    }
  }

  def setUserPreference(path:String): Unit ={

    if(fileMap.getOrElse(path,true)){

      val bufferedSource = io.Source.fromFile(path)

      bufferedSource.getLines().foreach{
        line =>
          try{

            val user = userPreference.apply(line)

            if(user.uid > 0) {
              userMap.get(user.uid) match {
                case Some(data) =>
                  val newuser = data.dataList :+ user
                  userMap = userMap + (user.uid -> userDef(newuser))
                case None =>
                  userMap = userMap + (user.uid -> userDef(List(user)))
              }
            }

          }catch {
            case e: Exception =>
              println(e.getStackTrace)
          }
      }

      fileMap = fileMap + (path -> false)
      bufferedSource.close

    }
  }

  def setProductReference(path:String): Unit ={

    if(fileMap.getOrElse(path,true)){

      val bufferedSource = io.Source.fromFile(path)

      bufferedSource.getLines().foreach{
        line =>

          try{
            val cols = line.split("\t",-1)
            val pid = cols(0).toInt
            val score = cols(1).toInt

            productMap = productMap + (pid -> productDef(pid,score))

          }catch {
            case e:Exception =>
              println(e.getStackTrace)
          }
      }

      fileMap = fileMap + (path -> false)
      bufferedSource.close
    }
  }

  def flushUserMap(): Unit ={
    userMap = userMap.empty
  }

  def flushProductMap():Unit = {
    productMap = productMap.empty
  }

  def flushFileMap():Unit = {
    fileMap = fileMap.empty
  }

  private case class fileDef(reRead:Boolean,timestamp:Long = System.currentTimeMillis())

  private case class userDef(dataList:List[userPreference],timestamp:Long = System.currentTimeMillis())

  private case class productDef(pid:Int,score:Int,timestamp:Long = System.currentTimeMillis())

}
