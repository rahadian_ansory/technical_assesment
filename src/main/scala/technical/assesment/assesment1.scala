package technical.assesment

import technical.assesment.buffer.bufferManager

/**
  * Created by iqbal on 22/01/18.
  */
object assesment1 {

  def main(args: Array[String]): Unit = {

    val userId:Int = if(args.length > 0) args(0).toInt else 0

    println("Start Initializing")

    /* //UNCOMMENT TO USE FOLDER SCANNER FOR TSV FILE
    val userFileList = bufferManager.scanFolder("/mnt/data/user_pref/").filter(_.contains(".tsv"))
    val productFileList = bufferManager.scanFolder("/mnt/data/product/").filter(_.contains(".tsv"))
    productFileList.foreach(bufferManager.setProductReference)
    userFileList.foreach(bufferManager.setUserPreference)
    */

    bufferManager.setProductReference("./assesment1/product.tsv")
    bufferManager.setUserPreference("./assesment1/user.tsv")

    println("Done Initializing")

    val userReference = bufferManager.getUserPreference(userId)

    val recomenddedProduct = userReference.sortBy(_.finalScore).takeRight(5).reverse

    println(s"Recomended Product for user : ${userId} are : ")
    recomenddedProduct.foreach( num => println(num.pid))

    bufferManager.flushUserMap()
    bufferManager.flushProductMap()
    bufferManager.flushFileMap()


  }

}
