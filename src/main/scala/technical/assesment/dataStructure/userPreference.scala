package technical.assesment.dataStructure

import java.text.SimpleDateFormat
import java.time.LocalDate

/**
  * Created by iqbal on 22/01/18.
  */

object userPreference{

  def apply(lineRow:String): userPreference = {

    try{
      val cols = lineRow.split("\t",-1)
      val uid = cols(0).toInt
      val pid = cols(1).toInt
      val score = cols(2).toDouble
      val timestamp:Long = cols(3).toLong * 1000

      if(score <= 1 && score > -1){
        val finalScore = getFinalScore(score,timestamp)
        userPreference(uid, pid, score, timestamp,finalScore)
      }else{
        default()
      }

    }catch {
      case e: Exception =>
        default()
    }
  }


  def getFinalScore(score:Double,timestamp:Long):Double = {

    val byteScore = java.lang.Double.doubleToRawLongBits(score * 0.95)
    val finalByte = byteScore^dayDifference(timestamp)

    java.lang.Double.longBitsToDouble(finalByte)

  }

  def dayDifference(timestamp:Long):Long = {

    val df:SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
    val date:String = df.format(timestamp)
    val oldDate = LocalDate.parse(date)
    val newDate = LocalDate.now()

    newDate.toEpochDay - oldDate.toEpochDay

  }

  def default() : userPreference = {
    userPreference(0,0,0D,0L,0D)
  }

}

case class userPreference(uid:Int,pid:Int,score:Double,timestamp:Long,finalScore:Double) {

}